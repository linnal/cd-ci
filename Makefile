.PHONY: all test clean

test: ## Run tests
	docker-compose run app npm run test

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[93m%-30s\033[32m %s\n", $$1, $$2}'