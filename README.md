## Run

    docker-compose up

## WARNING

Don't keep `.env` file in the repo. It's here as it makes demo example simpler.

